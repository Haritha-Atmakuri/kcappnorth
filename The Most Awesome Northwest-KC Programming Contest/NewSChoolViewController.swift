//
//  NewSChoolViewController.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by student on 3/14/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class NewSChoolViewController: UIViewController {

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var coachTF: UITextField!
    @IBOutlet weak var warningMessage: UILabel!
    
    let schoolValue = Schools.shared

    @IBAction func done(_ sender: Any) {
        warningMessage.text = ""
        if nameTF.text == "" || coachTF.text == "" || nameTF == nil || coachTF == nil
        {
            warningMessage.text = "Please enter valid details to proceed forward"
        }
        else
        {
            warningMessage.text = ""
            schoolValue.saveSchool(name: nameTF.text!, coach: coachTF.text!)
        self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func cancel(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        warningMessage.text = ""
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
