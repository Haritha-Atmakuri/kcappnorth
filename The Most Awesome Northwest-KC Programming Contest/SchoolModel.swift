//
//  SchoolModel.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by student on 3/13/19.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let SchoolsRetrieved = Notification.Name("Schools Retrieved")
    static let TeamsForSelectedSchoolRetrieved = Notification.Name("Teams for Selected School Retrieved")
    static let TeamsRetrieved = Notification.Name("Teams Retrieved")
}


@objcMembers
class Team:NSObject
{
    var name:String?
    var students:[String]
    var objectId:String?
    
    init(name:String,students:[String])
    {
        self.name = name
        self.students = students
    }
    
    convenience override init()
    {
        self.init(name:"",students:[])
    }
    
    static func == (lhs: Team, rhs: Team) -> Bool {
        return lhs.name == rhs.name && lhs.students == rhs.students
    }
}

@objcMembers
class School : NSObject
{
    var name:String!
    var coach:String!
    var teams:[Team]!
    var objectId:String?
    
    init(name:String,coach:String,teams:[Team])
    {
        self.name = name
        self.coach = coach
        self.teams = []
    }
    
    convenience override init()
    {
        self.init(name:"",coach:"",teams:[])
    }
    
    static func == (lhs: School, rhs: School) -> Bool {
        return lhs.name == rhs.name && lhs.coach == rhs.coach && lhs.teams == rhs.teams
    }
    
    func addTeam(name:String,students:[String])
    {
        
        teams.append(Team(name: name, students: students))
    }
}


class Schools
{
    let backendless = Backendless.sharedInstance()!
    var schoolDataStore:IDataStore!
    var teamsDataStore:IDataStore!
    static var shared:Schools = Schools()
    
    var schools:[School] = []
    var teams:[Team] = []    //all teams in our model
    
    convenience init() {
        self.init(schools:[])
    }
    
    
    var selectedSchool:School?
    var teamsForSelectedSchool:[Team] = []
    
    
    private init(schools:[School]){
        schoolDataStore = backendless.data.of(School.self)                  // our connections to Backendless tables
        teamsDataStore = backendless.data.of(Team.self)
        
    }
    
    func numScools()->Int {
        return schools.count
    }
    
    func school(_ index:Int) -> School {
        return schools[index]
    }
    
    // this lets us use [ ] notation instead of the above
    
    subscript(index:Int) -> School {
        return schools[index]
    }
    
    func add(school:School){
        schools.append(school)
    }
    
    func delete(school:School){
        for i in 0 ..< schools.count {
            if schools[i] == school {
                schools.remove(at:i)
                break
            }
        }
        
    }
    
    func retrieveAllSchools() {
        let queryBuilder = DataQueryBuilder()
        queryBuilder!.setRelated(["teams"])
        queryBuilder!.setPageSize(100)
        Types.tryblock({() -> Void in
            self.schools = self.schoolDataStore.find(queryBuilder) as! [School]
            
        },
                       catchblock: {(fault) -> Void in print(fault ?? "Something has gone wrong  reloadingAllSchools()")}
        )
        
    }
    
    //    func retrieveAllSchoolsAsynchronously() {
    //        let queryBuilder = DataQueryBuilder()
    //        queryBuilder!.setRelated(["teams"]) // TouristSites referenced in City's touristSites
    //        // field will be retrieved for each City
    //        queryBuilder!.setPageSize(100) // up to 100 TouristSites can be retrieved for each City
    //             schoolDataStore.find(queryBuilder, response: {(results) -> Void in
    //            self.schools = results as! [School]
    //            NotificationCenter.default.post(name: .SchoolsRetrieved, object: nil) // broadcast a Notification that Cities have been retrieved
    //        }, error: {(exception) -> Void in
    //            print(exception.debugDescription)
    //        })
    //    }
    
    func retrieveAllTeams() {
        
        Types.tryblock({() -> Void in self.teams = self.teamsDataStore.find() as! [Team]}, catchblock: {(fault) -> Void in print(fault ?? "Something has gone wrong when retrieveAllTeams()")})
        
    }
    
    func retrieveAllTeamsAsynchronously() {
        let startDate = Date()
        teamsDataStore.find({
            (retrievedTeams) -> Void in
            self.teams = retrievedTeams as! [Team]
            NotificationCenter.default.post(name: .TeamsRetrieved, object: nil) // broadcast a Notification that tourist sites have been retrieved
        },
                            error: {(exception) -> Void in
                                print(exception.debugDescription)
        })
        print("Done in \(Date().timeIntervalSince(startDate)) seconds ")
    }
    
    func retrieveTeamsForSelectedSchoolAsynchronously() {
        let startDate = Date()
        let queryBuilder:DataQueryBuilder = DataQueryBuilder()
        queryBuilder.setWhereClause("name = '\(self.selectedSchool?.name ?? "")'" ) // restrict ourselves to one city
        queryBuilder.setPageSize(100)
        queryBuilder.setRelated( ["teams"] )
        self.schoolDataStore.find(queryBuilder,
                                  
                                  response: {(results) -> Void in
                                    
                                    let school = results![0] as! School
                                    
                                    self.teamsForSelectedSchool = school.teams
                                    
                                    NotificationCenter.default.post(name: .TeamsForSelectedSchoolRetrieved, object: nil) // broadcast the fact that tourist sites for selected city have been retrieved
                                    
        }, error: {(exception) -> Void in
            
            print(exception.debugDescription)
            
        })
        
        print("Done in \(Date().timeIntervalSince(startDate)) seconds ")
        
    }
    
    //    func saveSchoolAsynchronously(named name:String, coach:String) {
    //        var schoolToSave = School(name: name, coach: coach, teams: [])
    //        schoolDataStore.save(schoolToSave, response: {(result) -> Void in
    //            schoolToSave = result as! School
    //            self.schools.append(schoolToSave)
    //            self.retrieveAllSchoolsAsynchronously()},
    //                             error:{(exception) -> Void in
    //                                print(exception.debugDescription)
    //        })
    //    }
    
    func saveSchool(name:String,coach:String)
    {
        var schoolToSave = School(name: name, coach: coach, teams: [])
        schoolToSave = schoolDataStore.save(schoolToSave) as! School
        schools.append(schoolToSave)
    }
    
    
    func saveTeamForSelectedSchool(school:School, team:Team) {
        print("Saving the team for the selected school...")
        let startingDate = Date()
        Types.tryblock({
            let savedTeam = self.teamsDataStore.save(team) as! Team // save one of its TouristSites
            self.schoolDataStore.addRelation("team:team:n", parentObjectId:   school.objectId, childObjects: [savedTeam.objectId!])
        }, catchblock:{ (exception) -> Void in
            print(exception.debugDescription)
        })
        print("Done!! in \(Date().timeIntervalSince(startingDate)) seconds")
    }
    
    func deleteSchool(school:School)
    {
     schoolDataStore.remove(school)
        for i in 0 ..< schools.count{
            if schools[i] == school
            {
            schools.remove(at:i)
            }
        }
    }
}
