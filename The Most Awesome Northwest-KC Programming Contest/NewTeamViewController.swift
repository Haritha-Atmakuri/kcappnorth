//
//  NewTeamViewController.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by student on 3/14/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class NewTeamViewController: UIViewController {

    @IBOutlet weak var warningMessage: UILabel!
    @IBOutlet weak var teamName: UITextField!
    @IBOutlet weak var student3TF: UITextField!
    @IBOutlet weak var student2TF: UITextField!
    @IBOutlet weak var student1TF: UITextField!
    @IBOutlet weak var getSchoolName: UILabel!
    
    let teamValue = Schools.shared
    var getSchool:School!
    
    @IBAction func cancel(_ sender: Any) {
        warningMessage.text = ""
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func done(_ sender: Any) {
        warningMessage.text = ""
        if teamName.text == "" || student3TF.text == "" || student2TF.text == "" || student1TF.text == ""
        {
            warningMessage.text = "Please enter valid details to proceed forward"
        }
        else
        {
       getSchool.addTeam(name: teamName.text!, students: [student1TF.text!,student2TF.text!,student3TF.text!])
            teamValue.saveTeamForSelectedSchool(school: getSchool, team: Team(name: teamName.text!, students: [student1TF.text!,student2TF.text!,student3TF.text!]))
        self.dismiss(animated: true, completion: nil)
        }
    }
    override func viewDidLoad() {
        getSchoolName.text = getSchool.name
        warningMessage.text = ""
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
