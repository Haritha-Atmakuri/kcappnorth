//
//  TeamsTVC.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by student on 3/14/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class TeamsTVC: UITableViewController {

    var school:School!
    var teamValue : Schools!
    
    override func viewDidLoad() {
        navigationItem.title = school.name
        super.viewDidLoad()
        teamValue = Schools.shared
         NotificationCenter.default.addObserver(self, selector: #selector(teamsForSelectedSchoolRetrieved), name: .TeamsForSelectedSchoolRetrieved, object: nil)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    @objc func teamsForSelectedSchoolRetrieved() {
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return school.teams.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamsList", for: indexPath)
        cell.textLabel?.text = school.teams[indexPath.row].name
        return cell
    }       
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = teamValue.selectedSchool?.name!
        teamValue.retrieveTeamsForSelectedSchoolAsynchronously()
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "getStudents" {
            let schoolDetails = segue.destination as! StudentsVC
            schoolDetails.studentDetails = school.teams[tableView.indexPathForSelectedRow!.row]
        }
        else if segue.identifier == "addNewTeam" {
            let newTeam = segue.destination as! NewTeamViewController
            newTeam.getSchool = school
        }
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            school.teams.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

}
