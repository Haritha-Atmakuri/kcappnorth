//
//  SchoolsTVC.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by student on 3/14/19.
//  Copyright � 2019 student. All rights reserved.
//

import UIKit

class SchoolsTVC: UITableViewController {

    var schoolValue : Schools!
    override func viewDidLoad() {
        super.viewDidLoad()
        schoolValue = Schools.shared
        schoolValue.retrieveAllSchools()
        NotificationCenter.default.addObserver(self, selector: #selector(schoolsRetrived), name: .SchoolsRetrieved, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        schoolValue.retrieveAllSchools()
        self.tableView.reloadData()
    }
    
    
    @objc func schoolsRetrived(){
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Schools.shared.numScools()
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "schoolcell")
        cell?.textLabel?.text = Schools.shared.school(indexPath.row).name
        cell?.detailTextLabel?.text = Schools.shared.school(indexPath.row).coach
        return cell!
    }
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "getTeams" {
            let teamDetails = segue.destination as! TeamsTVC
            teamDetails.school = Schools.shared[tableView.indexPathForSelectedRow!.row]
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //Schools.shared.delete(school: Schools.shared[indexPath.row] )
            schoolValue.deleteSchool(school: Schools.shared[indexPath.row] )
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    

}
