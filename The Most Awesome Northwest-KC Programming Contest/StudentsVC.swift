//
//  StudentsVC.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by student on 3/14/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class StudentsVC: UIViewController {

    @IBOutlet weak var student2UTF: UILabel!
    @IBOutlet weak var student1UTF: UILabel!
    @IBOutlet weak var student0UTF: UILabel!
    var studentDetails:Team!
    override func viewDidLoad() {
        navigationItem.title = studentDetails.name
        student0UTF.text = studentDetails.students[0]
        student1UTF.text = studentDetails.students[1]
        student2UTF.text = studentDetails.students[2]
        super.viewDidLoad()
        
    }
    


}
